import React from 'react';
import ReactRadioButtonGroup from 'react-radio-button-group';

class ReactCheckButtonGroup extends ReactRadioButtonGroup {
    constructor(props) {
        super(props);
    }

    getComponentName() {
        return 'react-check-button-group';
    }

    getPropsValues() {
        if (Array.isArray(this.props.value))
            return this.props.value;
        else if (this.props.value)
            return [this.props.value];
        return [];
    }

    createInitialState(props) {
        return {selectedValues: this.getPropsValues()};
    }

    willStateChange() {
        return true;
    }

    fireCurrentState() {
        if (this.props.onChange) {
            this.props.onChange(this.state.selectedValues.sort());
        }
    }

    fireEventValue(event) {
        if (this.props.onChange) {
            this.props.onChange(this.updateValuesWithEvent(this.getPropsValues(), event).sort());
        }
    }

    updateValuesWithEvent(values, event) {
        const changedValue = event.target.value;
        const checked = event.target.checked;

        let updatedValues = [...values];

        if (! checked)
            updatedValues = updatedValues.filter(value => value !== changedValue);
        else
            updatedValues.push(changedValue);

        return updatedValues;
    }

    createStateFromEvent(event) {
        return {
            selectedValues: this.updateValuesWithEvent(this.state.selectedValues, event)
        };
    }

    getInputType() {
        return "checkbox";
    }

    getIsValueChecked(value) {
        return (this.isStateful() && this.state.selectedValues.indexOf(value) >= 0) ||
            (! this.isStateful() && this.getPropsValues().indexOf(value) >= 0);
    }
}

ReactCheckButtonGroup.propTypes = ReactRadioButtonGroup.propTypes;
ReactCheckButtonGroup.propTypes.value = React.PropTypes.oneOfType([
    React.PropTypes.string,
    React.PropTypes.arrayOf(React.PropTypes.string)
]);

export default ReactCheckButtonGroup;