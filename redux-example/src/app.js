import React from 'react';
import {setNumbers} from './actions';
import ReactCheckButtonGroup from 'react-check-button-group';
import {connect} from 'react-redux';

class AppDisconnected extends React.Component {
    constructor(props) {
        super(props);
        this.handleCheckGroupChange = this.handleCheckGroupChange.bind(this);
    }

    handleCheckGroupChange(newValues) {
        this.props.setNumbers(newValues);
    }

    render() {
        const checkProps = {
            options: ['One', 'Two'],
            name: 'numbers',
            value: this.props.numbers,
            onChange: this.handleCheckGroupChange
        };
        return (
            <form action="/" method="GET">
                <ReactCheckButtonGroup {...checkProps}/>

                <input type="submit"/>
                <div>
                    {window.location.search}
                </div>
            </form>
        );
    }
};

const mapDispatchToProps = (dispatch) => ({
    setNumbers: (numbers) => dispatch(setNumbers(numbers))
});

const mapStateToProps = (state) => {
    return ({
        numbers: state.numbers
    });
};

const App = connect(
    mapStateToProps,
    mapDispatchToProps
)(AppDisconnected);

export default App;
