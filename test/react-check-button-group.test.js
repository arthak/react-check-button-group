import React from 'react';
import ReactCheckButtonGroup from '../src/react-check-button-group';
import ReactRadioButtonGroup from 'react-radio-button-group';
import renderer from 'react-test-renderer';
import {shallow} from 'enzyme';
import {mount} from 'enzyme';
import sinon from 'sinon';

test("Three text options yield three labels", () => {
    expect(
        renderer.create(
            <ReactCheckButtonGroup
                name='media'
                options={["TV", "Radio", "Youtube"]}/>
        ).toJSON()
    ).toMatchSnapshot();
});

test("Two values provided mean two checked inputs", () => {
    expect(
        renderer.create(
            <ReactCheckButtonGroup
                name='media'
                options={["TV", "Radio", "Youtube"]}
                value={["Radio", "Youtube"]}/>
        ).toJSON()
    ).toMatchSnapshot();
});

test("CSS classes all placed accordingly", () => {
    expect(
        renderer.create(
            <ReactCheckButtonGroup
                name='media'
                options={["TV", "Radio", "Youtube"]}
                value="Radio"
                groupClassName='cssGroup' itemClassName='cssItem'
                labelClassName='cssLabel' inputClassName='cssInput'/>
        ).toJSON()
    ).toMatchSnapshot();
});

test("CSS in option is stronger than general CSS", () => {
    expect(
        renderer.create(
            <ReactCheckButtonGroup
                name='media'
                options={
                [
                    {
                        value: "TV",
                        itemClassName: "cssItemTV",
                        labelClassName: "cssLabelTV"
                    },
                    {
                        value: "Youtube",
                        inputClassName: "cssInputYoutube"
                    },
                    "Radio"
                ]}
                value="Radio"
                groupClassName='cssGroup' itemClassName='cssItem'
                labelClassName='cssLabel' inputClassName='cssInput'/>
        ).toJSON()
    ).toMatchSnapshot();
});

test("Label is used when present, not value", () => {
    expect(
        renderer.create(
            <ReactCheckButtonGroup
                name='media'
                options={[
                    { value: "TV",      label: "Television" },
                    { value: "Youtube", label: "YouTube" },
                    { value: "Radio",   label: "Radio Station" }
                ]}/>
        ).toJSON()
    ).toMatchSnapshot();
});

test("fireOnMount set to true causes onChange when mounted", () => {
    const spy = sinon.spy();
    const group = mount(
        <ReactCheckButtonGroup
            name='media'
            options={["One", "Two", "Three"]}
            fireOnMount={true}
            isStateful={true}
            onChange={spy}
        />
    );
    expect(spy.callCount).toEqual(1);
    expect(spy.firstCall.args.length).toEqual(1);
    expect(spy.firstCall.args[0]).toEqual([]);
});

test("fireOnMount not fired when stateless mode", () => {
    const spy = sinon.spy();
    const group = mount(
        <ReactCheckButtonGroup
            name='media'
            options={["One", "Two", "Three"]}
            fireOnMount={true}
            onChange={spy}
        />
    );
    expect(spy.callCount).toEqual(0);
});

test("onChange works correctly when checkbox clicked", () => {
    const spy = sinon.spy();
    const group = mount(
        <ReactCheckButtonGroup
            name='media'
            options={["One", "Two", {value: "Three", inputClassName: "cssThree"}]}
            isStateful={true}
            onChange={spy}
        />
    );
    expect(spy.callCount).toEqual(0);
    group.find('.cssThree').simulate('change', {target: {checked: true, value: 'Three'}});
    expect(spy.callCount).toEqual(1);
    expect(spy.firstCall.args.length).toEqual(1);
    expect(spy.firstCall.args[0]).toEqual(['Three']);
});

test("Clicking on an input in stateless mode has no independent effect", () => {
    const spy = sinon.spy();
    const group = mount(
        <ReactCheckButtonGroup
            name='media'
            options={[
                { value: "One", inputClassName: "cssOne" },
                "Two",
                { value: "Three", inputClassName: "cssThree" }
            ]}
            value="Three"
            onChange={spy}
        />
    );
    group.find('.cssOne').simulate('change', {target: {checked: true, value: 'One'}});
    expect(spy.callCount).toEqual(1);
    expect(spy.firstCall.args.length).toEqual(1);
    expect(spy.firstCall.args[0]).toEqual(['One', 'Three']);

    expect(group.find('.cssOne').  props().checked).toBe(false);
    expect(group.find('.cssThree').props().checked).toBe(true);
});

test("Clicking on an input in stateful mode has immediate effect", () => {
    const spy = sinon.spy();
    const group = mount(
        <ReactCheckButtonGroup
            name='media'
            options={[
                { value: "One", inputClassName: "cssOne" },
                { value: "Two", inputClassName: "cssTwo" },
                { value: "Three", inputClassName: "cssThree" }
            ]}
            value={["One", "Two"]}
            isStateful={true}
            onChange={spy}
        />
    );
    expect(group.find('.cssOne').props().checked).toBe(true);
    expect(group.find('.cssTwo').props().checked).toBe(true);
    group.find('.cssThree').simulate('change', {target: {checked: true, value: 'Three'}});
    expect(spy.callCount).toEqual(1);
    expect(spy.firstCall.args.length).toEqual(1);
    expect(spy.firstCall.args[0]).toEqual(['One', 'Three', 'Two']);

    expect(group.find('.cssOne').  first().props().checked).toBe(true);
    expect(group.find('.cssTwo').first().props().checked).toBe(true);
    expect(group.find('.cssThree').first().props().checked).toBe(true);
});

test("Passing invalid onChange function does not crash and means no callback", () => {
    const group = mount(
        <ReactCheckButtonGroup
            name='media'
            options={["One", "Two", "Three"]}
            value="One"
            onChange={null}
        />
    );
    group.find('input').first().simulate('change', {target: {checked: true, value: 'One'}});
    expect(2 + 2).toEqual(4);
});
